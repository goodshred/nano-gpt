# 语法
pip install xxx -i https://mirrors.aliyun.com/pypi/simple

MacOS下使用xz解压缩：

xz -z 要压缩的文件
xz -d 要解压的文件

XZ 是一种使用LZMA2 压缩算法的压缩文件格式。 它被设计为流行的gzip 和bzip2 格式的替代品，并且与这些旧标准相比具有许多优势。 XZ 文件在许多平台上得到很好的支持，并且可以快速轻松地解压缩。 虽然它们不像ZIP 或RAR 文件那么常见，但XZ 存档可用于存储大量数据而不会牺牲压缩质量。


压缩文件夹：tar -cJf subset-1_data.xz subset-1_data




load_dataset详解

